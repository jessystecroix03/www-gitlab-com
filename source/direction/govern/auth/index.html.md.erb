---
layout: markdown_page
title: "Group Direction - Authorization and Authentication"
description: "Authentication and authorization keep resources secure but accessible."
---

- TOC
{:toc}

| Group | **Authentication and Authorization** |
| --- | --- |
| Stage | [Govern](https://about.gitlab.com/handbook/product/categories/#govern-stage) |
| Group | [Authentication and Authorization](https://about.gitlab.com/handbook/product/categories/#authentication-and-authorization-group) |
| Content Last Reviewed | `2023-09-06` |


<%= partial("direction/govern/auth/templates/intro") %>

## Overview

<%= partial("direction/govern/auth/templates/overview") %>

### Target audience and experience

<%= partial("direction/govern/auth/templates/target") %>

## Priorities

| Priority | Name | DRI | Target Release | 
| ------ | ------ | ------ | ------ |
<% data.product_priorities.authentication_authorization.priorities.each.with_index do |priority, index| %>
| <%= index + 1 %> | [<%= priority.name %>](<%= priority.url %>) | `<%= priority.dri.presence || "TBD" %>` | `<%= priority.target_release.presence || "TBD" %>`  |
<% end %>

## Where we are Headed

Security is top of mind for our customers, and by default, is also the highest priority for the Authentication and Authorization team. Our aim is to provide GitLab administrators the toolkit they need to make their GitLab experience both secure and accessible for their users - and to give them the flexibility to implement that in any way they choose. Just being able to security authenticate with GitLab is not enough. GitLab administrators need to secure all authentication avenues into the product, manage their users using automated processes, and have fine-grained control over what their users can and can't do in the product.

To be more specific, we are keenly aware of the increased security needs in Authentication and Authorization. The credentials that users are able to obtain are the "keys to the kingdom" and in a large enterprise, there needs to be strict inventory and management of these credentials, ensuring that their use and scope can be monitored, rotated, and revoked if necessary. In fact, there is even a shift away from using human-tied credentials altogether. They are too easily comprimised and prone to human error. Our team provides authentication solutions that aren't tied to humans to faciliate automated processes that have less security risk. We will continue to invest in making these non-human authentication methods easier to manage.

### What's Next and Why

<%= partial("direction/govern/auth/templates/next") %>

## Maturity Plan

<%= partial("direction/govern/auth/templates/maturity") %>

<%= partial("direction/govern/auth/templates/competitors") %>

## Goals

### Near-Term Goals

1. **Customizable Roles** - We are collaborating with other teams internally to add customizable permissions in their specific feature areas. Currently, we are working with the [Threat Insights](https://about.gitlab.com/handbook/engineering/development/sec/govern/threat-insights/) team. These permissions are security related - view them in [this](https://gitlab.com/groups/gitlab-org/-/epics/10684) epic.

We have most of the basics of the customizable roles [UI](https://gitlab.com/groups/gitlab-org/-/epics/9827) complete, and will start rolling it out in the 16.3 timeframe. Today, customizable roles are managed only via the API.

2. **Enterprise Users** We are putting the final touches on our [automated user claims process](https://gitlab.com/gitlab-org/gitlab/-/issues/322039), which means that any user that matches a verified domain and was not provisioned will automatically belong to the Enterprise. This is more secure than purely relying on SAML and SCIM provisioning, and also eliminates onboarding friction for new customers - if a domain is verified, those users will be automatically claimed. 


3. Enhance our [token rotation](https://gitlab.com/groups/gitlab-org/-/epics/11155) support to allow our users to avoid any unexpected surprises when [token lifetime limits](https://gitlab.com/gitlab-org/gitlab/-/issues/369123) are enforced in Summer 2024. 

### Mid-Term Goals

1. Continue to add customizable permissions to the framework, with [contributions from other teams](https://gitlab.com/groups/gitlab-org/-/epics/10000). Work on permissions that exist between Developer and Maintainer so that Maintainer is less likely to need to be assigned. This is a role that is extremely privileged, but with many permissions that aren't actually used - making it risky from a security and compliance perspective. Splitting off some of the permissions from the current Maintainer solve some of these issues.

2. Begin to tackle some of the token related [issues](https://gitlab.com/dashboard/issues?sort=updated_desc&state=opened&label_name[]=group::authentication+and+authorization&label_name[]=WG::TokenMgmt) that emerged as high reward, low effort from the [Token Management Working Group](https://about.gitlab.com/company/team/structure/working-groups/token-management/).


### Top Vision Item(s)

<%= partial("direction/govern/auth/templates/vision") %>
