---
layout: handbook-page-toc
title: "Field Team Recognition Programs"
description: "Field Team Recognition Programs aim to recognize Field team members for significant contributions to team performance. These programs are a direct result of Sales engagement survey data and CRO leaders' commitment to fostering a culture of recognition"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### Why Recognition? 

Why is recognition so important? [Research shows](https://greatergood.berkeley.edu/article/item/how_gratitude_can_transform_your_workplace) that gratitude fundamentally alters our brain chemistry and primes us for more compassion and happiness.

When it comes to work, [Harvard Business Review](https://hbr.org/2020/10/use-gratitude-to-counter-stress-and-uncertainty) shares that gratitude can help improve coworker relationships, increase perseverance through difficult tasks, and make teams more collaborative. Specifically: 
1. Improved mental wellness
1. Resilience to stress 
1. Helps with team bonding
1. Positively impacts our ability to make hard decisions and express self-control
1. Increased retention 
   - 63% of employees who are recognized are very unlikely to look for a new job ([source](https://www.hrtechnologist.com/articles/rewards-and-recognition/employee-recognition-and-retention-statistics/#))
1. Increased trust 
   - 50% of employees believe being thanked by managers not only improved their relationship but also built trust with their managers/executives ([source](https://www.tinypulse.com/blog/sk-employee-recognition-stats))

It's important to note that these results rely on a steady gratitude practice over a longer period, which means **we must make recognition a daily practice for our teams**.

## Field Quarterly Achiever's Chat 

The Quarterly Achiever's Chat recognizes Field team members who have excelled in a given quarter by inviting them to join the CEO and CRO in a congratulatory chat. The benefit is three-fold:
1. Top performers receive facetime and recognition directly at the executive level
1. Knowledge sharing of best practices that can replicated by other Field team members
1. Leaders/teams outside of the Field organization gain visibility into Sales momentum and key wins

Quarterly Achievers Chats span three quarters: Q1, Q2 and Q3 each year. We do not have Quarterly Achievers criteria/winners for Q4, as award/recognition efforts for Q4 are wrapped into Sales Kickoff (SKO) Awards.

Key features of this call include: 
1. 50-minute call
1. Held within first two months of new quarter
1. CEO and CRO attendance required
1. Not recorded 

### Selection Criteria - FY24-Q1

In total, 12 team members from across the Field organization will be recognized. The categories by segment for FY24-Q1 include:

**Enterprise Sales**
1. Land with Vision, Expand with Purpose Award
1. Leader of the Quarter Award
1. Services Attach Success Award

**Commercial Sales**
1. R7 Results Delivery Award
1. FY24-Q1 COMM Rookie Award

**Customer Success**
1. Stellar Services Results Award
1. SA Customer Results Award
1. Customer Adoption and Retention Award

**Partner** 
1. Channel First Order Award
1. Channel Management Focus Award
1. Alliances Peak Results Award

**Field Operations**
1. FOPS MVP

You can see a full description of each award [in this spreadsheet](https://docs.google.com/spreadsheets/d/1aEHcpaMb6tisRRJQtjBSG3UeVmPrdxTk13IZBUCind8/edit#gid=0).

Considerations include: 
1. Categories are reevaluated by CRO leadership and Field Operations every fiscal year and changed if deemed necessary. 
1. A team member cannot win a Quarterly Achiever award twice within a fiscal year. 

### Quarterly Achiever's Chat Process and Timeline
1. Quarterly Achievers finalized within the second week of the new quarter
1. Quarterly Achievers announced and recognized in the first WW Field Sales Call following [QBRs](/handbook/sales/qbrs/) (typically 3 weeks into the new quarter)
1. Quarterly Achievers Chat held within the first month of new quarter 
1. CEO and CRO to congratulate and inquire about key learnings from achievers on call
1. CEO and CRO to pull out 2-3 of the most impactful key learnings and share with the larger team in a Slack post (either #ceo or #cro) after the call
1. Achievers recognized in the next-available [Field Flash](/handbook/sales/field-communications/field-flash-newsletter/)

### Quarterly Achiever's Recognition Dinner 
As part of this initiative, Quarterly Achievers are able to expense a meal up to $250.00 USD within the same quarter they were selected in. For example, if a team member is selected as a Quarterly Achiever in November, they must expense their meal before the end of Q4 (the last day in January). Please submit your meal expense in Navan and in the description field link to this section of the handbook for approval. 

The $250.00 USD is reimbursable for a meal only, other items do not qualify. 




### Additional Resources for Recognition

You can find more guidance on recognition and what is "recognition worthy" at GitLab [in this handbook page](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#recognition).

Some additional resources include: 
1. [The Positive Power of Gratitude for Remote Teams](https://www.heykona.com/post/the-positive-power-of-gratitude-for-remote-teams) - Kona Blog 


